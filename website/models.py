from django.db import models

# Create your models here.

class Author(models.Model):
    """ This class represents the person that sends a comment. """

    name = models.CharField(max_length=20)
    email = models.EmailField(null=True, blank=True)
    geek = models.BooleanField(default=True)

    def __unicode__(self):
        return 'I am {0.name}'.format(self)

    def __str__(self):
        return 'I am {0.name}'.format(self)


class Message(models.Model):
    """ This class represents the message sended by a person.  """

    message = models.TextField(max_length=250)
    created_at = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(Author)

    def __unicode__(self):
        return 'Message: "{0.message}", authored by {0.author.name}'.format(self)

    def __str__(self):
        return 'Message: "{0.message}", authored by {0.author.name}'.format(self)


    class Meta:
        ordering = ['-created_at']
