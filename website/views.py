from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout
from website.models import Author, Message
from website.forms import MessageForm

# Create your views here.

def index(request):
    context = {}
    context['messages_form'] = MessageForm()
    if request.method == 'POST':
        form = MessageForm(request.POST)
        if form.is_valid():
            form.save()
        else:
            # Returns the same form to show the errors
            context['messages_form'] = form
    # Always returns a message_form in order to send new messages
    context['messages'] = Message.objects.all()

    return render(request, 'base/index.html', context)

@login_required
def secret(request):
    context = {}
    return render(request, 'base/secret.html', context)

def logout_view(request):
    context = {}
    logout(request)
    return render(request, 'registration/logout.html', context)
