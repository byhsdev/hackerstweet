from django import forms
from website.models import Message, Author

class MessageForm(forms.ModelForm):
    """ 
    ModelForm for Message model, It handles automagically the 
    business logic
    """

    author = forms.ModelChoiceField(queryset=Author.objects.all())

    class Meta:
        model = Message
        fields = ['message', 'author']
        help_texts = {
            'message': 'Write your message here. Max 250 characters.',
            'author': 'Select the author of this message from the list',
                }
        error_messages = {
                'message': {
                    'max_length': 'This message is too long, make a resume!',
                    },
                }
