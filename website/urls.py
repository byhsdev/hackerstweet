from django.conf.urls import patterns, url

urlpatterns = patterns('website.views',
    url(r'secret', 'secret', name='secret'),
    url(r'logout', 'logout_view', name='logout'),
    url(r'$', 'index', name='index'),
)
