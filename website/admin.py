from django.contrib import admin
from website.models import Author, Message

# Register your models here.

admin.site.register(Author)
admin.site.register(Message)
